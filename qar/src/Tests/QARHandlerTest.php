<?php

namespace Baine\QAR\Tests;

use Baine\QAR\Exceptions\ActionMissingExecuteMethodException;
use Baine\QAR\QARHandler;
use Baine\QAR\Tests\Fixtures\TestAction;
use Baine\QAR\Exceptions\ActionNotFoundException;
use Baine\QAR\Tests\Fixtures\Namespace\TestActionNamespace;

class QARHandlerTest extends TestCase
{
    public function test_overriding_actions()
    {
        QARHandler::$registeredActions = collect([TestActionNamespace::class, TestAction::class]);
        QARHandler::$registeredActionsBaseNamespace = "";
        $actions = QARHandler::getAllAvailableActions();
        $this->assertEquals(TestActionNamespace::class, $actions->first()->name);
        $this->assertEquals(TestAction::class, $actions->last()->name);
    }

    public function test_getting_action()
    {
        QARHandler::$registeredActionsBaseNamespace = "Baine\\QAR\\Tests\\Fixtures";
        $handler = new QARHandler('TestAction');
        $this->assertInstanceOf(TestAction::class, $handler->getCallableAction());
    }

    public function test_validating_action_has_execute_method()
    {
        QARHandler::$registeredActionsBaseNamespace = "Baine\\QAR\\Tests\\Fixtures";
        $handler = new QARHandler('NoExecuteAction');

        $this->expectException(ActionMissingExecuteMethodException::class);
        $handler->getCallableAction();
    }

    public function test_validating_action_exists()
    {
        QARHandler::$registeredActionsBaseNamespace = "Baine\\QAR\\Tests\\Fixtures";
        $handler = new QARHandler('TestActionDoesntExist');

        $this->expectException(ActionNotFoundException::class);
        $handler->getCallableAction();
    }

    public function test_getting_action_from_list()
    {
        QARHandler::$registeredActionsBaseNamespace = "Baine\\QAR\\Tests\\Fixtures";
        $handler = new QARHandler('TestAction');
        $this->assertInstanceOf(TestAction::class, $handler->getCallableAction());
    }

    public function test_turning_fwd_slash_to_back_slash()
    {
        QARHandler::$registeredActionsBaseNamespace = "Baine\\QAR\\Tests\\Fixtures";
        $handler = new QARHandler('Namespace/TestActionNamespace');
        $this->assertInstanceOf(TestActionNamespace::class, $handler->getCallableAction());
    }
}
