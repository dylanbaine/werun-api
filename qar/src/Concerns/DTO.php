<?php

namespace Baine\QAR\Concerns;

interface DTO
{
    public static function fromQAR(array $data): DTO;
}
