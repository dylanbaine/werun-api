<?php

namespace Baine\QAR;

use Exception;
use ReflectionClass;
use App\Concerns\DTO;
use ReflectionParameter;
use Illuminate\Support\Collection;
use Baine\QAR\Concerns\PrivateAction;
use Baine\QAR\Exceptions\ActionMissingExecuteMethodException;
use Baine\QAR\Exceptions\ActionNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class QARHandler
{
    protected $requestedAction;

    public static ?Collection $registeredActions = null;
    public static string $registeredActionsBaseNamespace;

    public function __construct(
        protected string $action,
        protected array $parameters = []
    ) {
    }

    public function getActionNamespace(): string
    {
        return static::$registeredActionsBaseNamespace . "\\" . str_replace('/', '\\', $this->action);
    }

    public static function getAllAvailableActions(): Collection
    {
        return static::$registeredActions
            ->map(function ($action) {
                $handler = new static($action);

                if (!$handler->getCallableAction()) {
                    return (object) ['name' => false];
                }

                $params = $handler->getExecuteMethodParameters();
                $returnType = $handler->getExecuteMethodReturnType();
                $action = [
                    'name' => $action,
                    'parameters' => [],
                    'returns' => $returnType ? $handler->getDTOPropertyInfo($returnType) : 'mixed',
                    'description' =>  $handler->getCallableAction()->description ?? null,
                ];
                foreach ($params as $param) {
                    if (class_exists($param->getType()->getName())) {
                        $action['parameters'][$param->name] = $handler->getDTOPropertyInfo($param->getType()->getName());
                    } else {
                        $action['parameters'][$param->name] = $param->getType()->getName();
                    }
                }
                return (object)$action;
            })
            ->where('name', '!=', false)
            ->values();
    }

    public function getCallableAction()
    {
        if ($this->requestedAction) {
            return $this->requestedAction;
        }

        $fullNamespace = $this->getActionNamespace();

        if (!class_exists($fullNamespace)) {
            throw new ActionNotFoundException("Action $fullNamespace could not be found.");
        }

        $handler = app()->make($fullNamespace);

        if ($handler instanceof PrivateAction) {
            return null;
        }

        if (!method_exists($handler, 'execute')) {
            throw new ActionMissingExecuteMethodException("Action " . get_class($handler) . " did not have the required `execute()` method.");
        }

        return $this->requestedAction = $handler;
    }

    /**
     *
     * @return ReflectionParameter[]
     */
    public function getExecuteMethodParameters(): array
    {
        $actionReflection = new ReflectionClass($this->getCallableAction());
        $execute = $actionReflection->getMethod('execute');
        return $execute->getParameters();
    }

    public function getExecuteMethodReturnType(): ?string
    {
        $method = (new ReflectionClass(get_class($this->getCallableAction())))
            ->getMethod('execute');
        $returnType = optional($method->getReturnType())->getName();
        return $returnType;
    }

    public function getMappedParameters(): array
    {
        $mappedParams = [];
        foreach ($this->getExecuteMethodParameters() as $param) {
            if (!isset($this->parameters[$param->name]) && !$param->isOptional()) {
                throw new Exception("Parameter {$param->name} is required");
            }
            if (isset($this->parameters[$param->name])) {
                $argDTOClass = $param->getType()->getName();
                $payloadValue = $this->parameters[$param->name];
                if (class_exists($argDTOClass) && isset(class_implements($argDTOClass)[DTO::class])) {
                    $payloadValue = $this->transformArgToDTO($argDTOClass, $payloadValue);
                }
                $mappedParams[$param->name] = $payloadValue;
            }
        }
        return $mappedParams;
    }

    public static function getDTOPropertyInfo(string $dto)
    {
        if (!class_exists($dto)) {
            return $dto;
        }

        try {
            $dtoInstance = new $dto;
            if ($dtoInstance instanceof Model) {
                $properties = array_filter(
                    $dtoInstance->getFillable(),
                    fn ($attr) => !in_array($attr, $dtoInstance->getHidden())
                );
                $data = [
                    $dtoInstance->getKeyName() => $dtoInstance->getKeyType(),
                ];
                foreach ($properties as $property) {
                    $data[$property] = 'string';
                }
                if ($dtoInstance->getCreatedAtColumn()) {
                    $data[$dtoInstance->getCreatedAtColumn()] = 'string';
                }
                if ($dtoInstance->getUpdatedAtColumn()) {
                    $data[$dtoInstance->getUpdatedAtColumn()] = 'string';
                }
                return $data;
            }
            if ($dtoInstance instanceof Collection) {
                return 'array';
            }
        } catch (Exception $e) {
        }

        $properties = [];
        $reflection = new ReflectionClass($dto);
        foreach ($reflection->getProperties() as $property) {
            if ($property->isStatic() || $property->isPrivate() || $property->isProtected()) {
                continue;
            }
            $value = optional($property->getType())->getName();
            if (class_exists($value)) {
                $value = static::getDTOPropertyInfo($value);
            }
            $properties[$property->name] = $value;
        }
        return $properties;
    }

    public function transformArgToDTO(string $argDTOClass, array $payloadValue)
    {
        $class = $argDTOClass::fromQAR($payloadValue);
        return $class;
    }

    public function execute()
    {
        return $this->getCallableAction()->execute(...$this->getMappedParameters());
    }
}
