<?php

namespace Baine\QAR;

use Exception;
use Baine\QAR\QARHandler;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class QARController extends Controller
{

    public function getActions()
    {
        return QARHandler::getAllAvailableActions();
    }

    public function __invoke(Request $request)
    {

        // If request has an `action` key in the request
        // parameters, run and return the
        // results of one action.
        if ($request->action) {
            return $this->doAction($request->action, $request->parameters ?? []);
        }

        // Build a response based on the keys in
        // the request parameters.
        $response = [];
        foreach ($request->all() as $name => $payload) {
            $response[$name] = $this->doAction($payload['action'], $payload['parameters'] ?? []);
        }
        return $response;
    }

    /**
     * Run the action's execute method with the
     * given `payload` as argumets.
     *
     * @param string $action
     * @param array|string $parameters
     * @return mixed
     */
    protected function doAction(string $action, $parameters)
    {
        if (!is_array($parameters)) {
            $parameters = json_decode($parameters, true);
        }

        if (!is_array($parameters)) {
            throw new Exception("Error when executing action `{$action}`. The `parameters` must be an array or json encoded object.", 422);
        }

        $handler = new QARHandler($action, $parameters);

        return $handler->execute();
    }
}
