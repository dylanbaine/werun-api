<?php

namespace Baine\QAR\Exceptions;

use Exception;

class ActionNotFoundException extends Exception
{
}
