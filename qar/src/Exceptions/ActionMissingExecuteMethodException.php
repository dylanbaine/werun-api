<?php

namespace Baine\QAR\Exceptions;

use Exception;

class ActionMissingExecuteMethodException extends Exception
{
}
