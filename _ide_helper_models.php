<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models\Friends{
/**
 * App\Models\Friends\FriendRelationship
 *
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRelationship newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRelationship newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRelationship query()
 */
	class FriendRelationship extends \Eloquent {}
}

namespace App\Models\Friends{
/**
 * App\Models\Friends\FriendRequest
 *
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|FriendRequest query()
 */
	class FriendRequest extends \Eloquent {}
}

namespace App\Models\Races{
/**
 * App\Models\Races\Race
 *
 * @property int $id
 * @property int $created_by_id
 * @property string $name
 * @property \Illuminate\Support\Carbon $start_time
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\Races\RaceFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Race newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Race newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Race query()
 * @method static \Illuminate\Database\Eloquent\Builder|Race whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Race whereCreatedById($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Race whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Race whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Race whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Race whereUpdatedAt($value)
 */
	class Race extends \Eloquent {}
}

namespace App\Models\Races{
/**
 * App\Models\Races\RaceInvitation
 *
 * @property int $id
 * @property int $race_id
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|RaceInvitation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RaceInvitation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RaceInvitation query()
 * @method static \Illuminate\Database\Eloquent\Builder|RaceInvitation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaceInvitation whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaceInvitation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaceInvitation whereRaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaceInvitation whereUpdatedAt($value)
 */
	class RaceInvitation extends \Eloquent {}
}

namespace App\Models\Races{
/**
 * App\Models\Races\RaceParticipant
 *
 * @property int $id
 * @property int $race_id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|RaceParticipant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RaceParticipant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RaceParticipant query()
 * @method static \Illuminate\Database\Eloquent\Builder|RaceParticipant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaceParticipant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaceParticipant whereRaceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaceParticipant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RaceParticipant whereUserId($value)
 */
	class RaceParticipant extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Sanctum\PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

