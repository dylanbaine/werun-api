<?php

namespace App\DTO\Race;

use Baine\QAR\Concerns\DTO;
use Illuminate\Support\Carbon;

class UpdateRaceDto implements DTO
{
    public int $id;
    public ?string $name = null;
    public ?Carbon $start_time = null;

    public static function fromQAR(array $payload): self
    {
        $instance = new static;
        $instance->id = $payload['id'];
        $instance->name = $payload['name'] ?? null;
        if (isset($payload['start_time'])) {
            $instance->start_time = new Carbon($payload['start_time']);
        }
        return $instance;
    }
}
