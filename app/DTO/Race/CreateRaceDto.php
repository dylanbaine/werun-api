<?php

namespace App\DTO\Race;

use Baine\QAR\Concerns\DTO;
use Illuminate\Support\Carbon;

class CreateRaceDto implements DTO
{

    public string $name;
    public ?int $created_by_id = null;
    public Carbon $start_time;

    public static function fromQAR(array $payload): self
    {
        $instance = new static;
        $instance->name = $payload['name'];
        if ($payload['created_by_id']) {
            $instance->created_by_id = $payload['created_by_id'];
        }
        $instance->start_time = new Carbon($payload['start_time']);
        return $instance;
    }
}
