<?php

namespace App\DTO\User;

use App\Models\User;
use Illuminate\Contracts\Support\Responsable;

class LoginAttemptResultDto implements Responsable
{
    public bool $success = false;
    public ?User $user = null;
    public ?string $token = null;


    public static function fromUserAndToken(User $user, string $token): self
    {
        $instance = new static;
        $instance->user = $user;
        $instance->token = $token;
        $instance->success = true;
        return $instance;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function toResponse($request)
    {
        return (array) $this;
    }
}
