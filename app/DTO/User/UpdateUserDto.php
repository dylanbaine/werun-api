<?php

namespace App\DTO\User;

use Baine\QAR\Concerns\DTO;

class UpdateUserDto implements DTO
{
    public int $id;
    public ?string $name = null;
    public ?string $email = null;
    public ?string $password = null;

    public static function fromQAR(array $payload): self
    {
        $instance = new static;
        $instance->id = $payload['id'];
        $instance->name = $payload['name'] ?? null;
        $instance->email = $payload['email'] ?? null;
        $instance->password = $payload['password'] ?? null;
        return $instance;
    }
}
