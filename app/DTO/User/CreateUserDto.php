<?php

namespace App\DTO;

use Baine\QAR\Concerns\DTO;

class CreateUserDto implements DTO
{
    public string $name;
    public string $email;
    public string $password;

    public static function fromQAR(array $payload): DTO
    {
        $insance = new static;
        $insance->name = $payload['name'];
        $insance->email = $payload['email'];
        $insance->password = $payload['password'];
        return $insance;
    }
}
