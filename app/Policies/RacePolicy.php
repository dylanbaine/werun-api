<?php

namespace App\Policies;

use Cache;
use App\Models\User;
use App\Models\Races\Race;
use App\Models\Races\RaceInvitation;
use App\Models\Races\RaceParticipant;
use Illuminate\Auth\Access\HandlesAuthorization;

class RacePolicy
{
    use HandlesAuthorization;

    public function before(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    protected static function userCreatedRace(User $user, Race $race): bool
    {
        return $user->id == $race->created_by_id;
    }

    public function acceptInvitation(User $user, Race $race)
    {
        return RaceInvitation::whereRaceId($race->id)
            ->whereEmail($user->email)->first(['id']) !== null;
    }

    public function runInRace(User $user, Race $race)
    {
        return Cache::remember("runInRace::$user->id/$race->id", 120, function () use ($user, $race) {
            return RaceParticipant::whereUserId($user->id)
                ->whereRaceId($race->id)->first(['id']) !== null;
        });
    }

    public function create(User $user)
    {
        return true;
    }

    public function invite(User $user, Race $race)
    {
        return static::userCreatedRace($user, $race);
    }

    public function update(User $user, Race $race)
    {
        return static::userCreatedRace($user, $race);
    }

    public function delete(User $user, Race $race)
    {
        return static::userCreatedRace($user, $race);
    }
}
