<?php

namespace App\Policies;

use App\Models\Friends\FriendRequest;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FriendRequestPolicy
{
    use HandlesAuthorization;

    public function create(User $user, int $invitedId)
    {
        $existing = FriendRequest::whereRequestedById($user->id)
            ->whereInvitedId($invitedId)
            ->first(['id']);

        return $existing === null;
    }

    public function delete(User $user, FriendRequest $invite)
    {
        return in_array($user->id, [
            $invite->requested_by_id,
            $invite->invited_id,
        ]);
    }
}
