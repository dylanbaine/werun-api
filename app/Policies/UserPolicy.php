<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before(User $user)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    public function viewAll(?User $user)
    {
        if (!$user || !$user->isAdmin()) {
            abort(404);
        }
        return true;
    }

    public function view(User $user, User $model)
    {
        return $user->id == $model->id;
    }

    public function update(User $user, User $model)
    {
        return $user->id == $model->id;
    }

    public function delete(User $user, User $model)
    {
        return $user->id == $model->id;
    }
}
