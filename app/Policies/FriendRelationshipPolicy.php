<?php

namespace App\Policies;

use App\Models\Friends\FriendRelationship;
use App\Models\Friends\FriendRequest;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FriendRelationshipPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function create(User $user, int $invitedById)
    {
        $existingInvite = FriendRequest::where('requested_by_id', $invitedById)
            ->where('invited_id', $user->id)
            ->first(['id']);
        // Can only accept an invite if the current
        // user is the one that is invited. (cannot accept an invite for someone that I invited)
        if (!$existingInvite) {
            return false;
        }

        $existingRelationship = FriendRelationship::whereUserIdLeft($user->id)
            ->orWhere('requested_by_id', $invitedById)
            ->first(['id']);
        return $existingRelationship === null;
    }

    public function delete(User $user, FriendRelationship $friendRelationship)
    {
        return in_array($user->id, [
            $friendRelationship->user_id_left,
            $friendRelationship->user_id_right,
        ]);
    }
}
