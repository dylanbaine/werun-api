<?php

namespace App\Models\Races;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RaceInvitation extends Model
{
    // use HasFactory;
    protected $fillable = [
        'email',
        'race_id',
    ];
}
