<?php

namespace App\Models\Races;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RaceParticipant extends Model
{
    // use HasFactory;
    protected $fillable = [
        'race_id',
        'user_id'
    ];
}
