<?php

namespace App\Models\Races;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Race extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'created_by_id',
        'start_time'
    ];
    protected $casts = [
        'start_time' => 'datetime'
    ];
}
