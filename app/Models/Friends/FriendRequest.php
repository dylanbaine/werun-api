<?php

namespace App\Models\Friends;

use Illuminate\Database\Eloquent\Model;

class FriendRequest extends Model
{
    protected $fillable = [
        'requested_by_id',
        'invited_id',
    ];
}
