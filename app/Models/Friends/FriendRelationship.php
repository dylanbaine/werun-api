<?php

namespace App\Models\Friends;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FriendRelationship extends Model
{
    // use HasFactory;

    protected $fillable = [
        'user_id_left',
        'user_id_right',
    ];
}
