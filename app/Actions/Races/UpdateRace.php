<?php

namespace App\Actions\Races;

use App\DTO\Race\UpdateRaceDto;
use App\Models\Races\Race;
use Gate;

class UpdateRace
{
    public function execute(UpdateRaceDto $raceData): Race
    {
        $race = Race::findOrFail($raceData->id);
        Gate::authorize('update', $race);

        if ($raceData->name) {
            $race->name = $raceData->name;
        }

        if ($raceData->start_time) {
            $race->start_time = $raceData->start_time;
        }

        if ($race->isDirty()) {
            $race->save();
            return $race->fresh();
        }
        return $race;
    }
}
