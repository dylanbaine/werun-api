<?php

namespace App\Actions\Races;

use Gate;
use App\Models\Races\Race;
use App\DTO\Race\CreateRaceDto;

class CreateRace
{
    public function execute(CreateRaceDto $raceData): Race
    {
        if (!$raceData->created_by_id) {
            Gate::authorize('create', Race::class);
            $raceData->created_by_id = auth()->id();
        }
        $race = Race::create([
            'name' => $raceData->name,
            'created_by_id' => $raceData->created_by_id,
            'start_time' => $raceData->start_time,
        ]);
        return $race;
    }
}
