<?php

namespace App\Actions\Races;

use Gate;
use App\Models\Races\Race;
use App\Models\Races\RaceParticipant;

class AcceptInvitationToRace
{
    public function execute(int $userId, int $raceId): void
    {
        Gate::authorize('acceptInvitation', Race::findOrFail($raceId));
        RaceParticipant::create([
            'race_id' => $raceId,
            'user_id' => $userId
        ]);
    }
}
