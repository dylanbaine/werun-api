<?php

namespace App\Actions\Races;

use App\Models\Races\Race;
use App\Models\Races\RaceInvitation;
use App\Models\User;
use Gate;

class InviteUserToRace
{
    public function execute(string $email, int $raceId): void
    {
        Gate::authorize('invite', Race::findOrFail($raceId));
        RaceInvitation::create([
            'email' => $email,
            'race_id' => $raceId
        ]);
    }
}
