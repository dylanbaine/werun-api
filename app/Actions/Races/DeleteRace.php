<?php

namespace App\Actions\Races;

use App\Models\Races\Race;
use Gate;

class DeleteRace
{
    public function execute(int $raceId): void
    {
        $race = Race::findOrFail($raceId);
        Gate::authorize('delete', $race);
        $race->delete();
    }
}
