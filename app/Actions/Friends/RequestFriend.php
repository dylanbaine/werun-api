<?php

namespace App\Actions\Friends;

use App\Models\Friends\FriendRequest;
use Auth;
use Gate;

class RequestFriend
{
    public function execute(int $userId): void
    {
        Gate::authorize('create', [FriendRequest::class, $userId]);
        FriendRequest::create([
            'requested_by_id' => Auth::id(),
            'invited_id' => $userId,
        ]);
    }
}
