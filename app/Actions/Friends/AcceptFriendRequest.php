<?php

namespace App\Actions\Friends;

use App\Models\Friends\FriendRelationship;
use Auth;
use Gate;

class AcceptFriendRequest
{
    public function execute(int $invitedById): void
    {
        Gate::authorize('create', [FriendRelationship::class, $invitedById]);
        FriendRelationship::create([
            'user_id_left' => Auth::id(),
            'user_id_right' => $invitedById,
        ]);
    }
}
