<?php

namespace App\Actions\Friends;

use App\Models\Friends\FriendRelationship;
use Auth;
use Gate;

class DeleteFriendRelationship
{
    public function execute(int $otherUserId): void
    {
        $relationship = FriendRelationship::where([
            'user_id_left' => Auth::id(),
            'user_id_right' => $otherUserId,
        ])->orWhere([
            'user_id_left' => $otherUserId,
            'user_id_right' => Auth::id(),
        ])->first();
        Gate::authorize('delete', $relationship);
        $relationship->delete();
    }
}
