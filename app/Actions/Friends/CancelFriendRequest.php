<?php

namespace App\Actions\Friends;

use App\Models\Friends\FriendRequest;
use Gate;

class CancelFriendRequest
{
    public function execute(int $invitedId): void
    {
        $friendRequest = FriendRequest::whereInvitedId($invitedId)->firstOrFail();
        Gate::authorize('delete', $friendRequest);
        $friendRequest->delete();
    }
}
