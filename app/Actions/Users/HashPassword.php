<?php

namespace App\Actions\Users;

use Illuminate\Support\Facades\Hash;
use Baine\QAR\Concerns\PrivateAction;

class HashPassword implements PrivateAction
{
    public function execute(string $password): string
    {
        return Hash::make($password);
    }
}
