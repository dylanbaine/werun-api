<?php

namespace App\Actions\Users;

use App\DTO\CreateUserDto;
use App\DTO\User\LoginAttemptResultDto;
use Auth;

class Register
{
    public function __construct(
        protected CreateUser $createUser,
        protected GenerateApiToken $generateApiToken
    ) {
    }

    public function execute(string $name, string $email, string $password): LoginAttemptResultDto
    {
        $userDto = new CreateUserDto;
        $userDto->name = $name;
        $userDto->email = $email;
        $userDto->password = $password;
        $created = $this->createUser->execute($userDto);
        Auth::login($created);
        return LoginAttemptResultDto::fromUserAndToken($created, $this->generateApiToken->execute($created));
    }
}
