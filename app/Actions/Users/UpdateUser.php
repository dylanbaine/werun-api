<?php

namespace App\Actions\Users;

use App\Models\User;
use App\DTO\User\UpdateUserDto;
use Baine\QAR\Concerns\PrivateAction;

class UpdateUser implements PrivateAction
{
    function __construct(
        protected HashPassword $hashPassword,
        protected FindUser $findUser
    ) {
    }

    public function execute(UpdateUserDto $userData): User
    {
        $user = $this->findUser->execute($userData->id);
        if ($userData->email) {
            $user->email = $userData->email;
        }
        if ($userData->name) {
            $user->name = $userData->name;
        }
        if ($userData->password) {
            $user->password = $this->hashPassword->execute($userData->password);
        }
        if ($user->isDirty()) {
            $user->save();
            return $user->refresh();
        }
        return $user;
    }
}
