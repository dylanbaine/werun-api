<?php

namespace App\Actions\Users;

use App\Models\User;

class GenerateApiToken
{
    public function execute(User $user, string $tokenName = 'default'): string
    {
        $token = $user->createToken($tokenName);
        return $token->plainTextToken;
    }
}
