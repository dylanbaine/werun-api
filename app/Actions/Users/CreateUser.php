<?php

namespace App\Actions\Users;

use App\Models\User;
use App\DTO\CreateUserDto;
use Baine\QAR\Concerns\PrivateAction;
use Validator;

class CreateUser implements PrivateAction
{

    public function __construct(
        protected HashPassword $hashPassword
    ) {
    }

    public function execute(CreateUserDto $userData): User
    {
        Validator::make((array) $userData, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ])->validate();
        $user = User::create([
            'name' => $userData->name,
            'email' => $userData->email,
            'password' => $this->hashPassword->execute($userData->password),
        ]);
        return $user;
    }
}
