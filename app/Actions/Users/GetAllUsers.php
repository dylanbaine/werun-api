<?php

namespace App\Actions\Users;

use App\Models\User;
use Gate;
use Illuminate\Support\Collection;

class GetAllUsers
{
    public function execute(): Collection
    {
        Gate::authorize('viewAll', User::class);
        return User::get();
    }
}
