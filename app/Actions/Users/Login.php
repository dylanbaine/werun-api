<?php

namespace App\Actions\Users;

use App\DTO\User\LoginAttemptResultDto;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class Login
{

    public function __construct(
        protected GenerateApiToken $generateApiToken
    ) {
    }

    public function execute(string $email, string $password, bool $invalidateOtherTokens = false): LoginAttemptResultDto
    {
        $authenticated = Auth::attempt(compact('email', 'password'));
        if ($authenticated) {
            Auth::login(
                $user = User::whereEmail($email)->first(),
                true
            );
            if ($invalidateOtherTokens) {
                $user->tokens()->delete();
            }
            return LoginAttemptResultDto::fromUserAndToken($user, $this->generateApiToken->execute($user));
        }
        return new LoginAttemptResultDto;
    }
}
