<?php

namespace App\Actions\Users;

use App\Models\User;

class EmailVerified
{
    public function execute(User $user): void
    {
        $user->email_verified_at = now();
        $user->save();
    }
}
