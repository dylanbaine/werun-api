<?php

namespace App\Actions\Users;

use App\Models\User;

class FindUser
{
    public function execute(int $id): User
    {
        return User::findOrFail($id);
    }
}
