<?php

namespace App\Actions;

class CheckHealth
{
    public string $description = "Returns a 200 status code and some data so you know the service is running.";

    public function execute(string $message = 'ok'): array
    {
        return compact('message');
    }
}
