<?php

namespace App\Providers;

use Baine\QAR\QARHandler;
use Illuminate\Support\ServiceProvider;
use Storage;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        QARHandler::$registeredActionsBaseNamespace = "App\\Actions";
        QARHandler::$registeredActions = collect(Storage::disk('actions')->allFiles())->sort()->map(function ($action) {
            return str_replace('.php', '', $action);
        });
    }
}
