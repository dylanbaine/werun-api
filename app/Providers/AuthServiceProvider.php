<?php

namespace App\Providers;

use App\Models\Friends\FriendRequest;
use App\Models\Friends\FriendRelationship;
use App\Models\Races\Race;
use App\Models\User;
use App\Policies\FriendRequestPolicy;
use App\Policies\FriendRelationshipPolicy;
use App\Policies\RacePolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{

    protected $policies = [
        Race::class => RacePolicy::class,
        User::class => UserPolicy::class,
        FriendRequest::class => FriendRequestPolicy::class,
        FriendRelationship::class => FriendRelationshipPolicy::class,
    ];

    public function boot()
    {
        $this->registerPolicies();
    }
}
