<?php

namespace Tests\Feature\Actions\Users;

use Tests\TestCase;
use App\Models\User;
use App\DTO\CreateUserDto;
use App\Actions\Users\Register;
use App\Actions\Users\CreateUser;
use App\Actions\Users\HashPassword;
use App\Actions\Users\GenerateApiToken;
use Auth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Validation\ValidationException;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    public function test_registering_and_getting_token()
    {
        $tokenGenerator = new class extends GenerateApiToken
        {
            public function execute(User $user, string $tokenName = 'default'): string
            {
                return 'token';
            }
        };

        $registeredUser = (new Register(new CreateUser(new HashPassword), $tokenGenerator))
            ->execute('test', 'test@test.com', 'super secret');

        $this->assertTrue(Auth::check());
        $this->assertEquals('token', $registeredUser->token);
    }

    public function test_validating_email()
    {
        $this->expectException(ValidationException::class);
        $this->app->make(Register::class)
            ->execute('test', 'not an email', 'super secret');
    }

    public function test_non_duplicate_email()
    {
        $this->app->make(Register::class)
            ->execute('test', 'test@test.com', 'super secret');

        $this->expectException(ValidationException::class);

        $this->app->make(Register::class)
            ->execute('test', 'test@test.com', 'super secret');
    }
}
