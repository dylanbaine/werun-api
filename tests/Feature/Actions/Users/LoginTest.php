<?php

namespace Tests\Feature\Actions\Users;

use App\Actions\Users\GenerateApiToken;
use App\Actions\Users\Login;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function test_example()
    {
        $user = User::factory()->create();

        $tokenGenerator = new class extends GenerateApiToken
        {
            public function execute(User $user, string $tokenName = 'default'): string
            {
                return 'token';
            }
        };
        $loginResult = (new Login($tokenGenerator))
            ->execute($user->email, 'password');

        $this->assertEquals($user->id, auth()->id());
        $this->assertEquals('token', $loginResult->token);
    }
}
