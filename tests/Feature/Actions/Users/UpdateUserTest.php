<?php

namespace Tests\Feature\Actions\Users;

use App\Actions\Users\FindUser;
use App\Actions\Users\HashPassword;
use App\Actions\Users\UpdateUser;
use App\DTO\User\UpdateUserDto;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    use RefreshDatabase;

    public function test_example()
    {
        $mockUser = User::factory()->create();

        $updateData = new UpdateUserDto;
        $updateData->id = $mockUser->id;
        $updateData->email = 'edit@test.com';
        (new UpdateUser(
            new HashPassword,
            new FindUser
        ))->execute($updateData);

        $this->assertEquals($updateData->email, $mockUser->fresh()->email);
    }
}
