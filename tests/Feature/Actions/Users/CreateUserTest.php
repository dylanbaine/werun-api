<?php

namespace Tests\Feature\Actions\Users;

use App\Actions\Users\CreateUser;
use App\Actions\Users\HashPassword;
use App\DTO\CreateUserDto;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateUserTest extends TestCase
{

    use RefreshDatabase;

    public function test_creating_user()
    {
        $userData = new CreateUserDto;
        $userData->name = 'test';
        $userData->email = 'test@test.com';
        $userData->password = 'test';

        (new CreateUser(new HashPassword()))
            ->execute($userData);

        $this->assertDatabaseHas('users', [
            'email' => $userData->email,
        ]);
    }
}
