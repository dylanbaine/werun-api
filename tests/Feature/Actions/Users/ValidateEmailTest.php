<?php

namespace Tests\Feature\Actions\Users;

use App\Actions\Users\EmailVerified;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ValidateEmailTest extends TestCase
{
    use RefreshDatabase;

    public function test_example()
    {
        $user = User::factory()->make([
            'email_verified_at' => null,
        ]);

        (new EmailVerified)
            ->execute($user);

        $this->assertEquals(now()->toDateString(), $user->fresh()->email_verified_at->toDateString());
    }
}
