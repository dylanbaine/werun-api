<?php

namespace Tests\Feature\Actions\Races;

use Tests\TestCase;
use App\Models\User;
use App\DTO\Race\CreateRaceDto;
use App\Actions\Races\CreateRace;
use Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreateRaceTest extends TestCase
{
    use RefreshDatabase;

    public function test_requires_logged_in_when_no_user_id()
    {
        $this->expectException(AuthorizationException::class);

        $raceData = new CreateRaceDto;
        $raceData->name = 'Race';
        $raceData->start_time = now();
        (new CreateRace())
            ->execute($raceData);
    }

    public function test_assignes_logged_in_user_id_when_logged_in()
    {
        $user = User::factory()->create();
        Auth::login($user);

        $raceData = new CreateRaceDto;
        $raceData->name = 'Race';
        $raceData->created_by_id = $user->id;
        $raceData->start_time = now();
        (new CreateRace())
            ->execute($raceData);

        $this->assertDatabaseHas('races', [
            'created_by_id' => $raceData->created_by_id,
            'name' => $raceData->name,
            'start_time' => $raceData->start_time->toDateTimeString(),
        ]);
    }

    public function test_storing_race()
    {

        $raceData = new CreateRaceDto;
        $raceData->name = 'Race';
        $raceData->created_by_id = 100;
        $raceData->start_time = now();
        (new CreateRace())
            ->execute($raceData);

        $this->assertDatabaseHas('races', [
            'created_by_id' => $raceData->created_by_id,
            'name' => $raceData->name,
            'start_time' => $raceData->start_time->toDateTimeString(),
        ]);
    }
}
