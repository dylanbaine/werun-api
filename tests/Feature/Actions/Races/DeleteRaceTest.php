<?php

namespace Tests\Feature\Actions\Races;

use Tests\TestCase;
use App\Models\User;
use App\Models\Races\Race;
use App\Actions\Races\DeleteRace;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DeleteRaceTest extends TestCase
{
    use RefreshDatabase;

    public function test_can_only_delete_own_race()
    {

        $this->expectException(AuthorizationException::class);

        $user1 = User::factory()->create();
        $race = Race::factory()->create(['created_by_id' => $user1->id]);

        $user2 = User::factory()->create();
        Auth::login($user2);

        (new DeleteRace)
            ->execute($race->id);
    }

    public function test_require_login_when_deleting()
    {

        $this->expectException(AuthorizationException::class);

        (new DeleteRace)
            ->execute(Race::factory()->create()->id);
    }

    public function test_cant_update_race_that_doesnt_exist()
    {
        $this->expectException(ModelNotFoundException::class);

        (new DeleteRace)
            ->execute(100);
    }


    public function test_deleting_race()
    {
        $user = User::factory()->create();
        Auth::login($user);
        $race = Race::factory()->create([
            'created_by_id' => $user->id,
        ]);

        (new DeleteRace)
            ->execute($race->id);

        $this->assertNull($race->fresh());
    }
}
