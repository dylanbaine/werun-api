<?php

namespace Tests\Feature\Actions\Races;

use App\Actions\Races\InviteUserToRace;
use App\Models\Races\Race;
use App\Models\Races\RaceInvitation;
use App\Models\User;
use Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InviteUserToRaceTest extends TestCase
{
    use RefreshDatabase;

    public function test_inviting_user_to_race()
    {
        $user = User::factory()->create();
        Auth::login($user);
        $race = Race::factory()->create(['created_by_id' => $user->id]);

        (new InviteUserToRace)
            ->execute('test@test.com', $race->id);

        $this->assertTrue(
            RaceInvitation::whereEmail('test@test.com')->whereRaceId($race->id)->exists()
        );
    }

    public function test_can_only_invite_if_logged_in()
    {
        $this->expectException(AuthorizationException::class);

        $race = Race::factory()->create();

        (new InviteUserToRace)
            ->execute('test@test.com', $race->id);
    }

    public function test_only_owner_of_race_can_send_invites()
    {
        $this->expectException(AuthorizationException::class);

        $user = User::factory()->create();
        Auth::login(User::factory()->create());
        $race = Race::factory()->create(['created_by_id' => $user->id]);

        (new InviteUserToRace)
            ->execute('test@test.com', $race->id);
    }
}
