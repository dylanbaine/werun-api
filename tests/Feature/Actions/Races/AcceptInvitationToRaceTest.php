<?php

namespace Tests\Feature\Actions\Races;

use App\Actions\Races\AcceptInvitationToRace;
use App\Actions\Races\InviteUserToRace;
use App\Models\Races\Race;
use App\Models\Races\RaceInvitation;
use App\Models\Races\RaceParticipant;
use App\Models\User;
use Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AcceptInvitationToRaceTest extends TestCase
{
    use RefreshDatabase;

    public function test_storing_invitation_requires_logged_in_user()
    {
        $this->expectException(AuthorizationException::class);

        (new AcceptInvitationToRace)
            ->execute(1, Race::factory()->create()->id);
    }

    public function test_can_only_accept_invitation_if_logged_in()
    {
        $this->expectException(AuthorizationException::class);
        Auth::login($user = User::factory()->create());

        (new AcceptInvitationToRace)
            ->execute($user->id, Race::factory()->create()->id);
    }

    public function test_can_only_accept_invite_for_existing_race()
    {
        $this->expectException(ModelNotFoundException::class);
        Auth::login($user = User::factory()->create());

        (new AcceptInvitationToRace)
            ->execute($user->id, 999);
    }

    public function test_storing_race()
    {
        Auth::login($user = User::factory()->create());
        $race = Race::factory()->create();
        (new InviteUserToRace)
            ->execute($user->email, $race->id);

        (new AcceptInvitationToRace)
            ->execute($user->id, $race->id);

        $this->assertTrue(
            RaceParticipant::whereUserId($user->id)->whereRaceId($race->id)->exists()
        );
    }

    public function test_checking_if_can_run_in_race()
    {
        Auth::login($user = User::factory()->create());
        $race = Race::factory()->create();
        (new InviteUserToRace)
            ->execute($user->email, $race->id);
        (new AcceptInvitationToRace)
            ->execute($user->id, $race->id);

        $this->assertTrue($user->can('runInRace', $race));
        $this->assertFalse(User::factory()->create()->can('runInRace', $race));
    }
}
