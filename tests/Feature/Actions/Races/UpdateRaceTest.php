<?php

namespace Tests\Feature\Actions\Races;

use App\Actions\Races\UpdateRace;
use App\DTO\Race\UpdateRaceDto;
use App\Models\Races\Race;
use App\Models\User;
use Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateRaceTest extends TestCase
{
    use RefreshDatabase;

    public function test_can_only_edit_own_race()
    {

        $this->expectException(AuthorizationException::class);

        $user1 = User::factory()->create();
        $race = Race::factory()->create(['created_by_id' => $user1->id]);

        $user2 = User::factory()->create();
        Auth::login($user2);

        $raceData = new UpdateRaceDto;
        $raceData->id = $race->id;
        $raceData->start_time = now()->addHour();
        (new UpdateRace)
            ->execute($raceData);
    }

    public function test_require_login_when_updating()
    {

        $this->expectException(AuthorizationException::class);

        $raceData = new UpdateRaceDto;
        $raceData->id = Race::factory()->create()->id;
        $raceData->start_time = now()->addHour();
        (new UpdateRace)
            ->execute($raceData);
    }

    public function test_cant_update_race_that_doesnt_exist()
    {
        $this->expectException(ModelNotFoundException::class);

        $raceData = new UpdateRaceDto;
        $raceData->id = 1;
        $raceData->start_time = now()->addHour();
        (new UpdateRace)
            ->execute($raceData);
    }

    public function test_updating_race_start_time()
    {
        $user = User::factory()->create();
        Auth::login($user);
        $fakeRace = Race::factory()->create([
            'created_by_id' => $user->id,
        ]);

        $raceData = new UpdateRaceDto;
        $raceData->id = $fakeRace->id;
        $raceData->start_time = now()->addHour();
        (new UpdateRace)
            ->execute($raceData);

        $this->assertEquals($raceData->start_time->toDateTimeString(), $fakeRace->fresh()->start_time->toDateTimeString());
    }

    public function test_updating_race_name()
    {
        $user = User::factory()->create();
        Auth::login($user);
        $fakeRace = Race::factory()->create([
            'created_by_id' => $user->id,
        ]);

        $raceData = new UpdateRaceDto;
        $raceData->id = $fakeRace->id;
        $raceData->name = 'edit';
        (new UpdateRace)
            ->execute($raceData);

        $this->assertEquals($raceData->name, $fakeRace->fresh()->name);
    }
}
