<?php

namespace Tests\Feature\Actions\Friends;

use App\Actions\Friends\CancelFriendRequest;
use App\Actions\Friends\RequestFriend;
use App\Models\Friends\FriendRequest;
use App\Models\User;
use Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CancelFriendRequestTest extends TestCase
{

    use RefreshDatabase;

    public function test_deleting_friend_request_as_inviter()
    {
        $user = User::factory()->create();
        Auth::login($user);
        $invited = User::factory()->create();
        (new RequestFriend)
            ->execute($invited->id);

        (new CancelFriendRequest)
            ->execute($invited->id);

        $this->assertFalse(
            FriendRequest::whereInvitedById($user->id)
                ->whereInvitedId($invited->id)
                ->exists()
        );
    }

    public function test_deleting_friend_request_as_invited()
    {
        $user = User::factory()->create();
        Auth::login($user);
        $invited = User::factory()->create();
        (new RequestFriend)
            ->execute($invited->id);
        Auth::login($invited);

        (new CancelFriendRequest)
            ->execute($invited->id);

        $this->assertFalse(
            FriendRequest::whereInvitedById($user->id)
                ->whereInvitedId($invited->id)
                ->exists()
        );
    }

    public function test_random_people_cannot_remove_friend_request()
    {
        $user = User::factory()->create();
        Auth::login($user);
        $invited = User::factory()->create();
        (new RequestFriend)
            ->execute($invited->id);
        $random = User::factory()->create();
        Auth::login($random);

        $this->expectException(AuthorizationException::class);
        (new CancelFriendRequest)
            ->execute($invited->id);
    }
}
