<?php

namespace Tests\Feature\Actions\Friends;

use App\Actions\Friends\AcceptFriendRequest;
use App\Actions\Friends\DeleteFriendRelationship;
use App\Actions\Friends\RequestFriend;
use App\Models\Friends\FriendRelationship;
use App\Models\Friends\FriendRequest;
use App\Models\User;
use Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteFriendRelationshipTest extends TestCase
{
    use RefreshDatabase;

    public function test_can_only_delete_if_member_of_relationship()
    {
        $friendA = User::factory()->create();
        $friendB = User::factory()->create();
        $random = User::factory()->create();

        Auth::login($friendA);
        (new RequestFriend)
            ->execute($friendB->id);

        Auth::login($random);

        $this->expectException(AuthorizationException::class);
        (new DeleteFriendRelationship)
            ->execute($friendB->id);
    }

    public function test_can_delete_relationship()
    {
        $friendA = User::factory()->create();
        $friendB = User::factory()->create();

        Auth::login($friendA);
        (new RequestFriend)
            ->execute($friendB->id);
        Auth::login($friendB);
        (new AcceptFriendRequest)
            ->execute($friendA->id);

        (new DeleteFriendRelationship)
            ->execute($friendA->id);

        $this->assertFalse(
            FriendRelationship::where([
                'user_id_left' => $friendB->id,
                'user_id_right' => $friendA->id,
            ])->orWhere([
                'user_id_right' => $friendB->id,
                'user_id_left' => $friendA->id,
            ])->exists()
        );
    }
}
