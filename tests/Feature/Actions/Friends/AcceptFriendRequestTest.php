<?php

namespace Tests\Feature\Actions\Friends;

use App\Actions\Friends\AcceptFriendRequest;
use App\Actions\Friends\RequestFriend;
use App\Models\Friends\FriendRelationship;
use App\Models\User;
use Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AcceptFriendRequestTest extends TestCase
{

    use RefreshDatabase;

    public function test_cant_create_relationship_if_no_invite()
    {
        $user = User::factory()->create();
        Auth::login($user);
        $invitedById = User::factory()->create();

        $this->expectException(AuthorizationException::class);

        (new AcceptFriendRequest)
            ->execute($invitedById->id);
    }

    public function test_creating_relationship()
    {
        // log in as user A
        $invitedBy = User::factory()->create();
        Auth::login($invitedBy);

        // user A invites user B
        $user = User::factory()->create();
        (new RequestFriend)
            ->execute($user->id);

        // log in as user B and accept the friend request
        Auth::login($user);
        (new AcceptFriendRequest)
            ->execute($invitedBy->id);

        // ensure that the relationship exists
        $this->assertTrue(
            FriendRelationship::where([
                'user_id_left' => $user->id,
                'user_id_right' => $invitedBy->id,
            ])->orWhere([
                'user_id_right' => $user->id,
                'user_id_left' => $invitedBy->id,
            ])->exists()
        );
    }
}
