<?php

namespace Tests\Feature\Actions\Friends;

use App\Actions\Friends\RequestFriend;
use App\Models\Friends\FriendRequest;
use App\Models\User;
use Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RequestFriendTest extends TestCase
{
    use RefreshDatabase;

    public function test_sending_invite_to_be_friends()
    {
        $user = User::factory()->create();
        Auth::login($user);
        $toInvite = User::factory()->create();

        (new RequestFriend())
            ->execute($toInvite->id);

        $this->assertTrue(
            FriendRequest::whereRequestedById($user->id)->whereInvitedId($toInvite->id)->exists()
        );
    }

    public function test_cannot_duplicate_invite()
    {
        $user = User::factory()->create();
        Auth::login($user);
        $toInvite = User::factory()->create();
        (new RequestFriend())
            ->execute($toInvite->id);

        $this->expectException(AuthorizationException::class);
        (new RequestFriend())
            ->execute($toInvite->id);
    }
}
